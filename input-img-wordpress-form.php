<?php
/*
Plugin Name:Input Img Wordpress Form
Plugin URI: https://gitlab.com/franciscoblancojn/input-img-wordpress-form
Description: Generated ShortCode for Wordpress for show Input File of Img (use [IWIF_inputFile])
Author: Francisco Blanco
Version: 1.1.6
Author URI: https://franciscoblanco.vercel.app/
*/

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/franciscoblancojn/input-img-wordpress-form',
	__FILE__,
	'input-img-wordpress-form'
);
$myUpdateChecker->setAuthentication('MqiAAZzo9WNBAGzgwc3L');
$myUpdateChecker->setBranch('master');

function IWIF_get_version() {
    $plugin_data = get_plugin_data( __FILE__ );
    $plugin_version = $plugin_data['Version'];
    return $plugin_version;
}

define("IWIF_LOG",false);
define("IWIF_PATH",plugin_dir_path(__FILE__));
define("IWIF_URL",plugin_dir_url(__FILE__));

require_once IWIF_PATH . "src/_index.php";