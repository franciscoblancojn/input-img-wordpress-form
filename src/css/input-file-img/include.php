<?php

function IWIF_input_file_img_add_css() {
    wp_enqueue_style( 'IWIF_input_file_img_css',IWIF_URL."src/css/input-file-img/style.css", array(), IWIF_get_version(), 'all');
}
add_action( 'wp_enqueue_scripts', 'IWIF_input_file_img_add_css' );