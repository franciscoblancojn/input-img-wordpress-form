<?php

function IWIF_input_file_img_add_js() {
    wp_enqueue_script( 'IWIF_input_file_img_css',IWIF_URL."src/js/input-file-img/main.js", array(),  IWIF_get_version(), 'all');
}
add_action( 'wp_enqueue_scripts', 'IWIF_input_file_img_add_js' );
