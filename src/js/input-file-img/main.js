const IWIF_close_SVG = `
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512"><path d="M310.6 361.4c12.5 12.5 12.5 32.75 0 45.25C304.4 412.9 296.2 416 288 416s-16.38-3.125-22.62-9.375L160 301.3L54.63 406.6C48.38 412.9 40.19 416 32 416S15.63 412.9 9.375 406.6c-12.5-12.5-12.5-32.75 0-45.25l105.4-105.4L9.375 150.6c-12.5-12.5-12.5-32.75 0-45.25s32.75-12.5 45.25 0L160 210.8l105.4-105.4c12.5-12.5 32.75-12.5 45.25 0s12.5 32.75 0 45.25l-105.4 105.4L310.6 361.4z" fill="currentColor"/></svg>
`;

const IWIF_validateAccept = (nameFile) => {
  const extend = nameFile.split(".").pop();
  if (!["jpg", "jpeg"].includes(extend)) {
    return false;
  }
  return true;
};

const IWIF_updateProgress = (e) => {
  const loadingPercentage = ((100 * e.loaded) / e.total).toFixed(0);
  console.log(loadingPercentage);
};
const IWIF_toBase64 = (file) =>
  new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.addEventListener("progress", IWIF_updateProgress);
    reader.onload = () => resolve(reader.result);
    reader.onerror = (error) => reject(error);
  });

const IWIF_sendImg = async (base64) => {
  try {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var raw = JSON.stringify({
      image: base64,
    });

    var requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    const respond = await fetch(
      "https://inference.mooveri.com/api/v1/inference",
      requestOptions
    );
    const result = await respond.json();
    if (result.length == 0) {
      throw {};
    }
    return result;
  } catch (error) {
    alert("No se pudo subir la img, intentelo de nuevo");
    return false;
  }
};

const IWIF_removeImg = (e) => {
  while (!e.classList.value.includes("imgUpload")) {
    e = e.parentElement;
  }
  const countfile = e.parentElement.parentElement.children[0].children[2];
  var number = countfile.innerHTML;
  number = number == "" ? 0 : parseInt(number);
  number--;
  countfile.innerHTML = number;
  if (number == 0) {
    countfile.classList.remove("show");
  }

  e.outerHTML = "";
};
const IWIF_addNewImg = async (contentFile, url) => {
  const img = url.split(";base64,");

  const imgResult = await IWIF_sendImg(img[1]);
  if (imgResult !== false) {
    for (let i = 0; i < imgResult.length; i++) {
      const ImgRespond = imgResult[i];
      const newImg = ImgRespond.image;
      delete ImgRespond.image;

      contentFile.innerHTML = `
        ${contentFile.innerHTML}
        <div class="imgUpload">
          <img src="${img[0] + ";base64," + newImg}"/>
          <div class="removeImg" onclick="IWIF_removeImg(this)">${IWIF_close_SVG}</div>
          <input type="hidden" name="IWIF_inputFile[]" value="${window.btoa(
            JSON.stringify(ImgRespond)
          )}"/>
        </div>
      `;
    }
  }
  return imgResult;
};

const IWIF_showInputFile = async (url, e) => {
  const label = e.parentElement;
  const contentFile = label.parentElement.children[1];

  const result = await IWIF_addNewImg(contentFile, url);
  if (result !== false) {
    const countfile = label.children[2];
    countfile.classList.add("show");
    var number = countfile.innerHTML;
    number = number == "" ? 0 : parseInt(number);
    number += result.length;
    countfile.innerHTML = number;
  }
};

const IWIF_uploadFile = async (e) => {
  const file = e.target.files[0];
  const fileUrl = await IWIF_toBase64(file);
  const nameFile = e.target.value.split("\\").pop();
  if (IWIF_validateAccept(nameFile)) {
    await IWIF_showInputFile(fileUrl, e.target);
  } else {
    alert("File Invalid");
  }
};

const IWIF_inputFile_function = (IWIF_inputFile) => {
  IWIF_inputFile.addEventListener("change", IWIF_uploadFile);
};

const IWIF_inputFile_load = () => {
  const IWIF_inputFile_all = document.querySelectorAll(
    ".IWIF_inputFile input[type='file']"
  );
  for (let i = 0; i < IWIF_inputFile_all.length; i++) {
    const IWIF_inputFile = IWIF_inputFile_all[i];
    IWIF_inputFile_function(IWIF_inputFile);
  }
};
window.addEventListener("load", IWIF_inputFile_load);
